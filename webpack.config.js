const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLWebPackPlugin = require('html-webpack-plugin');
const createStyledComponentsTransformer = require('typescript-plugin-styled-components')
  .default;

module.exports = (env = { NODE_ENV: 'development' }) => {
  const { NODE_ENV } = env;
  const pathToDist = path.resolve(__dirname, 'dist');
  const pathToSrc = path.resolve(__dirname, 'src');
  const styledComponentsTransformer = createStyledComponentsTransformer();

  return {
    mode: NODE_ENV,

    devtool: NODE_ENV === 'development' ? 'eval' : false,

    context: pathToSrc,

    entry: './index.tsx',

    output: {
      path: pathToDist,
      filename: 'build.js',
      publicPath: '/',
    },

    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    module: {
      rules: [
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          loader: 'ts-loader',
          options: {
            getCustomTransformers: NODE_ENV === 'development' ? () => ({
              before: [styledComponentsTransformer],
            }) : () => {},
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader'],
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]?[hash]',
          },
        },
        {
          enforce: 'pre',
          test: /\.js$/,
          use: {
            loader: 'source-map-loader',
          },
        },
      ],
    },

    devServer: {
      contentBase: pathToDist,
      port: 9000,
      watchContentBase: true,
      historyApiFallback: true,
    },

    plugins: [
      new MiniCssExtractPlugin({ filename: 'index.css' }),
      new HTMLWebPackPlugin({
        filename: path.resolve(pathToDist, 'index.html'),
        hash: true,
        template: path.resolve(pathToSrc, 'index.html'),
      }),
    ],
  };
};
