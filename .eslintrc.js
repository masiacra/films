module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": ["airbnb", 'plugin:jest/recommended', 'jest-enzyme', "plugin:@typescript-eslint/recommended"],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "NODE_ENV": false
    },
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "plugins": [
        "babel",
        "react",
        "react-hooks"
    ],
    "rules": {
        "linebreak-style": "off",
        "no-console": "off",
        "react/jsx-filename-extension": ["error", { "extensions": [".js", ".tsx"] }],
        "@typescript-eslint/camelcase":"off",
        '@typescript-eslint/explicit-function-return-type': "off",
        "import/no-unresolved": "off",
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn"
    }
};
