import { Film } from '../films/films.types';

export enum SearchBy {
  title = 'title',
  genre = 'genre'
}

export enum SortBy {
  release_date = 'release date',
  rating = 'rating'
}

export interface ReduxState {
  isFetching: boolean;
  films: Film[];
  activeFilm: Film | null;
  sortBy: SortBy;
  searchBy: SearchBy;
  isError: boolean;
  isActiveFilmError: boolean;
  isActiveFilmFetching: boolean;
}
