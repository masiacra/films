import reducer, { initialState } from './reducer';
import { Film } from '../films/films.types';
import Actions from '../actions/actions.types';
import { SortBy, SearchBy } from './reducer.types';

describe('reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, { type: null })).toEqual(initialState);
  });
  it('should handle films request', () => {
    expect(reducer(initialState, { type: Actions.GET_FILMS_REQUEST }))
      .toEqual({ ...initialState, isFetching: true });
  });
  it('should handle films success response', () => {
    const payload: Film[] = [
      {
        id: 212,
        title: 'shrek',
        tagline: '',
        vote_average: 3,
        release_date: '21017_212_21',
        poster_path: '',
        overview: '',
        budget: 120,
        revenue: 1,
        runtime: 120,
        genres: [],
      },
      {
        id: 213,
        title: 'shrek2',
        tagline: '',
        vote_average: 3,
        release_date: '21017_212_21',
        poster_path: '',
        overview: '',
        budget: 120,
        revenue: 1,
        runtime: 120,
        genres: [],
      },
    ];
    expect(reducer(initialState, { type: Actions.GET_FILMS_SUCCESS, payload }))
      .toEqual({ ...initialState, films: payload, isFetching: false });
  });

  it('should handle films failure response', () => {
    expect(reducer(initialState, { type: Actions.GET_FILMS_FAILURE }))
      .toEqual({ ...initialState, isFetching: false, isError: true });
  });
  it('should handle activeFilm request', () => {
    expect(reducer(initialState, { type: Actions.GET_ACTIVE_FILM_REQUEST }))
      .toEqual({ ...initialState, isActiveFilmFetching: true });
  });
  it('should handle activeFilm success response', () => {
    const payload: Film = {
      id: 212,
      title: 'shrek',
      tagline: '',
      vote_average: 3,
      release_date: '21017_212_21',
      poster_path: '',
      overview: '',
      budget: 120,
      revenue: 1,
      runtime: 120,
      genres: [],
    };
    expect(reducer(initialState, { type: Actions.GET_ACTIVE_FILM_SUCCESS, payload }))
      .toEqual({ ...initialState, activeFilm: payload, isActiveFilmFetching: false });
  });
  it('should handle activeFilm failure response', () => {
    expect(reducer(initialState, { type: Actions.GET_ACTIVE_FILM_FAILURE }))
      .toEqual({ ...initialState, isActiveFilmFetching: false, isActiveFilmError: true });
  });

  it('should handle set sortBy', () => {
    const sortByParam = SortBy.release_date;
    expect(reducer(initialState, { type: Actions.SET_SORT_BY, payload: sortByParam }))
      .toEqual({ ...initialState, sortBy: sortByParam });
  });
  it('should handle set searchBy', () => {
    const searchByParam = SearchBy.genre;
    expect(reducer(initialState, { type: Actions.SET_SEARCH_BY, payload: searchByParam }))
      .toEqual({ ...initialState, searchBy: searchByParam });
  });
});
