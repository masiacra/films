import { AnyAction } from 'redux';
import {
  ReduxState,
  SearchBy,
  SortBy,
} from './reducer.types';
import Actions from '../actions/actions.types';


export const initialState: ReduxState = {
  isFetching: false,
  films: [],
  activeFilm: null,
  sortBy: SortBy.rating,
  isError: false,
  searchBy: SearchBy.title,
  isActiveFilmError: false,
  isActiveFilmFetching: false,
};

export default function reducer(
  state: ReduxState = initialState,
  action: AnyAction,
): ReduxState {
  switch (action.type) {
    case Actions.SET_SEARCH_BY:
      return { ...state, searchBy: action.payload };
    case Actions.SET_SORT_BY:
      return { ...state, sortBy: action.payload };
    case Actions.GET_ACTIVE_FILM_REQUEST:
      return { ...state, isActiveFilmFetching: true };
    case Actions.GET_ACTIVE_FILM_SUCCESS:
      return { ...state, isActiveFilmFetching: false, activeFilm: action.payload };
    case Actions.GET_ACTIVE_FILM_FAILURE:
      return { ...state, isActiveFilmFetching: false, isActiveFilmError: true };
    case Actions.GET_FILMS_REQUEST:
      return { ...state, isFetching: true };
    case Actions.GET_FILMS_SUCCESS:
      return { ...state, isFetching: false, films: action.payload };
    case Actions.GET_FILMS_FAILURE:
      return { ...state, isFetching: false, isError: true };
    default:
      return state;
  }
}
