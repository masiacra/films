import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import App from './app.component';
import Films from './films/films.component';
import Footer from './footer/footer.component';


it('App should render 1 component Films and 1 component Footer', () => {
  const wrapper = shallow(<App />);
  const find = wrapper.find.bind(wrapper);
  expect(find(Films)).toHaveLength(1);
  expect(find(Footer)).toHaveLength(1);
});
