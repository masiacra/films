import { AnyAction } from 'redux';
import {
  setSortBy,
  setSearchBy,
  getFilmsRequest,
  getFilmsSuccess,
  getFilmsFailure,
  getActiveFilmRequest,
  getActiveFilmSuccess,
  getActiveFilmFailure,
} from './actions';
import Actions from './actions.types';
import { Film } from '../films/films.types';
import { SearchBy, SortBy } from '../reducer/reducer.types';

describe('actionCreators', () => {
  describe('should create an action', () => {
    it('to set sortBy param', () => {
      const sortByParam = SortBy.release_date;
      const expectedAction = {
        type: Actions.SET_SORT_BY,
        payload: sortByParam,
      };
      expect(setSortBy(sortByParam)).toEqual(expectedAction);
    });
    it('to set searchBy param', () => {
      const searchByParam = SearchBy.title;
      const expectedAction = {
        type: Actions.SET_SEARCH_BY,
        payload: searchByParam,
      };
      expect(setSearchBy(searchByParam)).toEqual(expectedAction);
    });
    it('to send request to load films from server', () => {
      const expectedAction = {
        type: Actions.GET_FILMS_REQUEST,
      };
      expect(getFilmsRequest()).toEqual(expectedAction);
    });
    it('to get success response from server to load films', () => {
      const expectedAction: AnyAction = {
        type: Actions.GET_FILMS_SUCCESS,
        payload: [],
      };
      const payload: Film[] = [];
      expect(getFilmsSuccess(payload)).toEqual(expectedAction);
    });
    it('to get failure response from server to load films', () => {
      const expectedAction = {
        type: Actions.GET_FILMS_FAILURE,
      };
      expect(getFilmsFailure()).toEqual(expectedAction);
    });
    it('to send request to load activeFilm', () => {
      const expectedAction = {
        type: Actions.GET_ACTIVE_FILM_REQUEST,
      };
      expect(getActiveFilmRequest()).toEqual(expectedAction);
    });
    it('to get success response from server to load activeFilm', () => {
      const payload: Film = {
        id: 1,
        tagline: '',
        title: '',
        vote_average: 3,
        release_date: '2019-8-11',
        poster_path: '',
        overview: '',
        budget: 3,
        revenue: 10,
        runtime: 120,
        genres: [],
      };
      const expectedAction = {
        type: Actions.GET_ACTIVE_FILM_SUCCESS,
        payload,
      };
      expect(getActiveFilmSuccess(payload)).toEqual(expectedAction);
    });
    it('to get failure response from server to load activeFilm', () => {
      const expectedAction = {
        type: Actions.GET_ACTIVE_FILM_FAILURE,
      };
      expect(getActiveFilmFailure()).toEqual(expectedAction);
    });
  });
});
