// eslint-disable-next-line import/no-extraneous-dependencies
import configureMockStore, { MockStore } from 'redux-mock-store';
import thunk from 'redux-thunk';
import { AnyAction } from 'redux';
import ACTIONS from './actions.types';
import {
  loadActiveFilm,
  loadFilms,
} from './films.resource';
import { Film } from '../films/films.types';
import { ReduxState, SearchBy } from '../reducer/reducer.types';

describe('async actions', () => {
  const filmsResponse = {
    data: [
      {
        id: 2,
        title: 'Shrek 2',
        tagline: '',
        vote_average: 4,
        release_date: '2004-11-02',
        poster_path: '',
        overview: 'it is overview about shrek2',
        budget: 120,
        revenue: 120,
        runtime: 120,
        genres: ['comedy'],
      },
      {
        id: 1,
        title: 'Shrek',
        tagline: '',
        vote_average: 5,
        release_date: '2000-11-02',
        poster_path: '',
        overview: 'it is overview about shrek',
        budget: 120,
        revenue: 120,
        runtime: 120,
        genres: ['comedy'],
      },
    ],
  };
  const filmResponse = filmsResponse.data[0];
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  interface State {
    films: Film[];
    isError: boolean;
    isFetching: boolean;
    searchBy: string;
    sortBy: string;
  }
  const initialState: State = {
    films: [],
    isError: false,
    isFetching: false,
    searchBy: 'genre',
    sortBy: 'rating',
  };
  const search = 'shrek';
  const searchBy = SearchBy.title;
  const url = 'https://reactjs-cdp.herokuapp.com/movies';
  const matchingStringForFilms = `${url
  }?search=${search}&searchBy=${searchBy}&sortBy=vote_average&sortOrder=asc`;
  const matchingStringForFilm = `${url}/1/`;
  let store: MockStore;
  let getActions: () => AnyAction[];
  let dispatch: (action: AnyAction) => ReduxState;
  beforeEach(() => {
    store = mockStore(initialState);
    getActions = store.getActions.bind(store);
    dispatch = store.dispatch.bind(store);
  });

  afterEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (global as any).fetch.mockRestore();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (global as any).fetch = null;
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const createFetchStub = (callback: () => Promise<any>) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (global as any).fetch = jest.fn(callback);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return (global as any).fetch.mock.calls;
  };

  it('creates GET_FILMS_SUCCESS when fetching is complete and have success', (done) => {
    const fetchCalls = createFetchStub(() => Promise.resolve({
      json: () => Promise.resolve(filmsResponse),
    }));
    const expectedActions = [
      {
        type: ACTIONS.GET_FILMS_REQUEST,
      },
      {
        type: ACTIONS.GET_FILMS_SUCCESS,
        payload: filmsResponse.data,
      },
    ];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch(loadFilms({ search, searchBy }) as any);
    process.nextTick(() => {
      expect(getActions()).toEqual(expectedActions);
      expect(fetchCalls.length).toEqual(1);
      expect(fetchCalls[0][0]).toEqual(matchingStringForFilms);
      done();
    });
  });
  it('creates GET_FILMS_FAILURE  when fetching is complete and have error', (done) => {
    createFetchStub(() => Promise.reject(new Error('404 not found')));
    const expectedActions = [
      {
        type: ACTIONS.GET_FILMS_REQUEST,
      },
      {
        type: ACTIONS.GET_FILMS_FAILURE,
      },
    ];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch(loadFilms({ search, searchBy }) as any);
    process.nextTick(() => {
      expect(getActions()).toEqual(expectedActions);
      done();
    });
  });

  it('creates GET_ACTIVE_FILM_SUCCESS when fetching is complete and have success', (done) => {
    const fetchCalls = createFetchStub(() => Promise.resolve({
      json: () => Promise.resolve(filmResponse),
    }));
    const expectedActions = [
      {
        type: ACTIONS.GET_ACTIVE_FILM_REQUEST,
      },
      {
        type: ACTIONS.GET_ACTIVE_FILM_SUCCESS,
        payload: filmResponse,
      },
    ];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch(loadActiveFilm('1') as any);
    process.nextTick(() => {
      expect(getActions()).toEqual(expectedActions);
      expect(fetchCalls.length).toEqual(1);
      expect(fetchCalls[0][0]).toEqual(matchingStringForFilm);
      done();
    });
  });
  it('creates GET_ACTIVE_FILM_FAILURE  when fetching is complete and have error', (done) => {
    createFetchStub(() => Promise.reject(new Error('404 not found')));
    const expectedActions = [
      {
        type: ACTIONS.GET_ACTIVE_FILM_REQUEST,
      },
      {
        type: ACTIONS.GET_ACTIVE_FILM_FAILURE,
      },
    ];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch(loadActiveFilm('1') as any);
    process.nextTick(() => {
      expect(getActions()).toEqual(expectedActions);
      done();
    });
  });
});
