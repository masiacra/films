import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { getUrl } from '../films/films.service';
import { ReduxState } from '../reducer/reducer.types';
import {
  getActiveFilmRequest,
  getActiveFilmSuccess,
  getActiveFilmFailure,
  getFilmsRequest,
  getFilmsSuccess,
  getFilmsFailure,
} from './actions';
import { FilmsParams } from './films.resource.types';

export function loadFilms({
  search,
  searchBy,
}: FilmsParams) {
  return (dispatch: ThunkDispatch<ReduxState, null, AnyAction>) => {
    const url: string = getUrl({
      search,
      searchBy,
    });
    dispatch(getFilmsRequest());
    fetch(url)
      .then((response) => response.json())
      .then((result) => {
        dispatch(getFilmsSuccess(result.data));
      })
      .catch(() => {
        dispatch(getFilmsFailure());
      });
  };
}

export function loadActiveFilm(id: string) {
  return (dispatch: ThunkDispatch<ReduxState, null, AnyAction>) => {
    const url = `https://reactjs-cdp.herokuapp.com/movies/${id}/`;
    dispatch(getActiveFilmRequest());
    fetch(url)
      .then((response) => response.json())
      .then((film) => {
        dispatch(getActiveFilmSuccess(film));
      })
      .catch(() => {
        dispatch(getActiveFilmFailure());
      });
  };
}
