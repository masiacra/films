import { SearchBy } from '../reducer/reducer.types';

export interface FilmsParams {
  search: string;
  searchBy: SearchBy;
}
