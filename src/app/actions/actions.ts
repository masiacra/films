import { AnyAction } from 'redux';
import { Film } from '../films/films.types';
import Actions from './actions.types';
import { SearchBy, SortBy } from '../reducer/reducer.types';

export function setSortBy(value: SortBy): AnyAction {
  return {
    type: Actions.SET_SORT_BY,
    payload: value,
  };
}

export function setSearchBy(value: SearchBy): AnyAction {
  return {
    type: Actions.SET_SEARCH_BY,
    payload: value,
  };
}

export function getFilmsRequest(): AnyAction {
  return {
    type: Actions.GET_FILMS_REQUEST,
  };
}

export function getFilmsSuccess(films: Film[]): AnyAction {
  return {
    type: Actions.GET_FILMS_SUCCESS,
    payload: films,
  };
}

export function getFilmsFailure(): AnyAction {
  return {
    type: Actions.GET_FILMS_FAILURE,
  };
}

export function getActiveFilmRequest(): AnyAction {
  return {
    type: Actions.GET_ACTIVE_FILM_REQUEST,
  };
}

export function getActiveFilmSuccess(film: Film): AnyAction {
  return {
    type: Actions.GET_ACTIVE_FILM_SUCCESS,
    payload: film,
  };
}

export function getActiveFilmFailure(): AnyAction {
  return {
    type: Actions.GET_ACTIVE_FILM_FAILURE,
  };
}

export function loadFilmsRequest(): AnyAction {
  return {
    type: Actions.GET_FILMS_REQUEST,
  };
}
