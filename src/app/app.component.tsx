import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Films from './films/films.component';
import Footer from './footer/footer.component';
import { store, persistor } from './store/store';

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Films />
        <Footer />
      </PersistGate>
    </Provider>
  );
}
