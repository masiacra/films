enum FilmsPaths {
  default = '/',
  notFound = '/notFound',
  search = '/search',
  film = '/film',
  nothingMatch = '/*'
}

export default FilmsPaths;
