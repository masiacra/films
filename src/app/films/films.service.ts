import { Film } from './films.types';
import { SearchBy } from '../reducer/reducer.types';

function getYear(date: string): string {
  return date.split('-')[0];
}

function getGenres(array: string[]) {
  return array.join(' & ');
}

function validateRuntime(runtime: number | null) {
  return runtime ? `${runtime} min` : '';
}

function sort<T>(compareFunction: (a: T, b: T) => number) {
  return function innerSort(array: T[]): T[] {
    return array.slice().sort(compareFunction);
  };
}

function ratingComparator({ vote_average: first }: Film, { vote_average: second }: Film): number {
  return first - second;
}

function releaseDateComparator(
  { release_date: first }: Film,
  { release_date: second }: Film,
): number {
  const firstDate = new Date(first);
  const secondDate = new Date(second);
  if (firstDate > secondDate) {
    return 1;
  }
  if (firstDate < secondDate) {
    return -1;
  }
  return 0;
}

const sortByReleaseDate: (films: Film[]) => Film[] = sort(releaseDateComparator);
const sortByRating: (films: Film[]) => Film[] = sort(ratingComparator);

type getUrlParams = {
  search: string;
  searchBy: string;
}
function getUrl({
  search,
  searchBy,
}: getUrlParams): string {
  const searchByParam: string = searchBy === SearchBy.title ? 'title' : 'genres';
  const url: URL = new URL('https://reactjs-cdp.herokuapp.com/movies');
  url.searchParams.set('search', search);
  url.searchParams.set('searchBy', searchByParam);
  url.searchParams.set('sortBy', 'vote_average');
  url.searchParams.set('sortOrder', 'asc');
  return url.toString();
}

function isValidActiveFilm(activeFilm: Film | null) {
  return (activeFilm && JSON.stringify(activeFilm) !== JSON.stringify({}));
}

export {
  getYear,
  getGenres,
  validateRuntime,
  sortByReleaseDate,
  sortByRating,
  getUrl,
  isValidActiveFilm,
};
