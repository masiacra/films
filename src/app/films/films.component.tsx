import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './header/header.component';
import TitleBar from './title_bar/title_bar.container';
import MoviesListWrapper from './movies_list/movies_list_wrapper.component';
import FilmsErrorBoundary from './films_error_boundary/films_error_boundary.component';
import NotFound from './not_found/not_found.component';
import FilmsPaths from '../app.types';

export default function Films() {
  return (
    <FilmsErrorBoundary>
      <BrowserRouter>
        <Switch>
          <Route path={FilmsPaths.notFound}>
            <NotFound />
          </Route>
          <Route path={FilmsPaths.default}>
            <Header />
            <TitleBar />
            <MoviesListWrapper />
          </Route>
        </Switch>
      </BrowserRouter>
    </FilmsErrorBoundary>
  );
}
