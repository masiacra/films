export interface Film {
  id: number;
  title: string;
  tagline: string;
  vote_average: number;
  release_date: string;
  poster_path: string | null;
  overview: string;
  budget: number;
  revenue: number;
  runtime: number | null;
  genres: string[];
}
