import {
  getGenres,
  sortByRating,
  getYear,
  sortByReleaseDate,
  validateRuntime,
  getUrl,
  isValidActiveFilm,
} from './films.service';
import { Film } from './films.types';
import { SearchBy } from '../reducer/reducer.types';

describe('getGenres', () => {
  it('should return string if input value is array', () => {
    expect(getGenres(['comedy', 'drama'])).toEqual('comedy & drama');
    expect(getGenres(['comedy'])).toEqual('comedy');
    expect(getGenres([])).toEqual('');
  });
});

describe('getYear', () => {
  it('should return year if input value is string and presented in the following format: "YYYY-DD-MM"', () => {
    expect(getYear('2017-10-10')).toEqual('2017');
  });
  it('should return the same string if input value is string', () => {
    expect(getYear('2017/10/10')).toEqual('2017/10/10');
  });
});

describe('validateRuntime', () => {
  it('should return value in format runtime + "min" if runtime is typeof string', () => {
    expect(validateRuntime(107)).toEqual('107 min');
  });
  it('should return empty string if input value is null', () => {
    expect(validateRuntime(null)).toEqual('');
  });
});

describe('sortByReleaseDate', () => {
  it('should return sorted array by release_date in asc format', () => {
    const input: Film[] = [
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2016',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2015',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2017',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2018',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
    ];
    const output: Film[] = [
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2015',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2016',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2017',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3,
        release_date: '2018',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
    ];
    expect(sortByReleaseDate(input)).toEqual(output);
  });
});

describe('sortByRating', () => {
  it('should return sorted array by vote_average in asc format', () => {
    const input: Film[] = [
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 4.5,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 5.2,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3.7,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3.2,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
    ];
    const output: Film[] = [
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3.2,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 3.7,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 4.5,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
      {
        id: 1,
        title: '',
        tagline: '',
        vote_average: 5.2,
        release_date: '',
        poster_path: null,
        overview: '',
        budget: 0,
        revenue: 0,
        runtime: 0,
        genres: [''],
      },
    ];
    expect(sortByRating(input)).toEqual(output);
  });
});

describe('commonHelperFunctions', () => {
  const urlString = 'https://reactjs-cdp.herokuapp.com/movies?search=shrek&';
  it('should set properly query string', () => {
    expect(getUrl({
      search: 'shrek',
      searchBy: SearchBy.title,
    }).toString()).toEqual(`${urlString}searchBy=title&sortBy=vote_average&sortOrder=asc`);
    expect(getUrl({
      search: 'shrek',
      searchBy: SearchBy.genre,
    }).toString()).toEqual(`${urlString}searchBy=genres&sortBy=vote_average&sortOrder=asc`);
  });
});

describe('isValidActiveFilm', () => {
  it('should return true if activeFilm is not null or empty object', () => {
    const film: Film = {
      id: 1,
      title: '',
      tagline: '',
      vote_average: 3,
      release_date: '',
      poster_path: '',
      overview: '',
      budget: 120,
      revenue: 3,
      runtime: 120,
      genres: [],
    };
    expect(isValidActiveFilm(film)).toBeTruthy();
  });
  it('should return false if activeFilm is null', () => {
    expect(isValidActiveFilm(null)).toBeFalsy();
  });
  it('should return false if activeFilm is empty object', () => {
    expect(isValidActiveFilm({} as Film)).toBeFalsy();
  });
});
