import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import Films from './films.component';
import FilmsErrorBoundary from './films_error_boundary/films_error_boundary.component';
import Header from './header/header.component';
import TitleBar from './title_bar/title_bar.container';
import MoviesListWrapper from './movies_list/movies_list_wrapper.component';

it('Films should render FilmErrorBoundary, Header, TitleBar, MoviesListWrapper when path="/"', () => {
  const wrapper = shallow(<Films />);
  const find = wrapper.find.bind(wrapper);
  expect(find(FilmsErrorBoundary)).toHaveLength(1);
  expect(find(Header)).toHaveLength(1);
  expect(find(TitleBar)).toHaveLength(1);
  expect(find(MoviesListWrapper)).toHaveLength(1);
  wrapper.unmount();
});
