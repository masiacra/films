import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import FilmCard, {
  Poster,
  Genres,
  ReleaseDate,
  Title,
} from './film_card.component';
import { Film } from '../../films.types';

describe('FilmCard', () => {
  const film: Film = {
    id: 1,
    tagline: '',
    vote_average: 2,
    overview: '',
    budget: 3,
    revenue: 2,
    runtime: 120,
    title: 'shrek',
    poster_path: null,
    release_date: '2000-10-12',
    genres: ['sci-fi'],
  };
  const wrapper = shallow(
    <FilmCard film={film} />,
  );
  const find = wrapper.find.bind(wrapper);
  it('should render properly', () => {
    expect(find(Poster)).toHaveLength(1);
    expect(find(Title)).toHaveLength(1);
    expect(find(ReleaseDate)).toHaveLength(1);
    expect(find(Genres)).toHaveLength(1);
  });
});
