import React, { memo } from 'react';
import styled from 'styled-components';
import { getGenres, getYear } from '../../films.service';
import { FilmCardProps } from './film_card.types';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const DEFAULT_IMAGE = require('../../../../images/mock_bison.jpg');

export const Poster = styled.img`
  max-width: 300px;
  height: auto;
`;

export const ReleaseDate = styled.p`
  border: 1px solid #ADD8E6;
  border-radius: 5px;
  flex: 0 1 auto;
`;

export const Genres = styled.p`
  font-size: 10px;
  flex: 0 1 auto;
`;

export const Title = styled.p`
  text-transform: uppercase;
  flex: 1 1 auto;
`;

const Row = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

export const FilmCardRoot = ({
  film,
}: FilmCardProps) => {
  const {
    poster_path,
    title,
    release_date,
    genres,
  } = film;
  return (
    <div tabIndex={0} role="button">
      <Poster
        src={poster_path || DEFAULT_IMAGE}
        alt={title}
      />
      <Row>
        <Title>
          {title}
        </Title>
        <ReleaseDate>
          {getYear(release_date)}
        </ReleaseDate>
      </Row>
      <Genres>{getGenres(genres)}</Genres>
    </div>
  );
};

const FilmCard = memo(FilmCardRoot);

export default FilmCard;
