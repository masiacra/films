import { Film } from '../../films.types';

export interface FilmCardProps {
  film: Film;
}
