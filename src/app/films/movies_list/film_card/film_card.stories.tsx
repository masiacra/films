import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
import { FilmCardRoot } from './film_card.component';
import { Film } from '../../films.types';


const filmWithoutPosterPath: Film = {
  id: 1,
  title: 'shrek',
  genres: ['comedy', 'fantasy'],
  tagline: 'some tagline',
  release_date: '2019-11-14',
  vote_average: 3,
  poster_path: null,
  overview: 'film about shrek',
  budget: 120000,
  revenue: 3,
  runtime: 120,
};

const filmWithPosterPath: Film = {
  ...filmWithoutPosterPath,
  poster_path: 'https://image.tmdb.org/t/p/w500/140ewbWv8qHStD3mlBDvvGd0Zvu.jpg',
};

storiesOf('FilmCard', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add('with film without poster_path', () => (
    <FilmCardRoot film={filmWithoutPosterPath} />
  ))
  .add('with film with poster_path', () => (
    <FilmCardRoot film={filmWithPosterPath} />
  ));
