import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { MemoryRouter } from 'react-router-dom';
import MoviesList from './movies_list.component';
import { Film } from '../films.types';
import FilmsPaths from '../../app.types';

const films: Film[] = [
  {
    id: 1,
    title: 'shrek',
    genres: ['comedy', 'fantasy'],
    tagline: 'some tagline',
    release_date: '2019-11-14',
    vote_average: 3,
    poster_path: null,
    overview: 'film about shrek',
    budget: 120000,
    revenue: 3,
    runtime: 120,
  },
  {
    id: 2,
    title: 'shrek2',
    genres: ['comedy', 'fantasy'],
    tagline: 'some tagline',
    release_date: '2019-11-15',
    vote_average: 3,
    poster_path: null,
    overview: 'film about shrek and donkey',
    budget: 120000,
    revenue: 3,
    runtime: 120,
  },
];

const activeFilm: Film = films[0];

const loadFilms = action('loading films');

storiesOf('MoviesList', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .addDecorator((story) => (
    <MemoryRouter initialEntries={[`${FilmsPaths.film}/1/`]}>
      {story()}
    </MemoryRouter>
  ))
  .add('initial state', () => (
    <MoviesList
      films={[]}
      activeFilm={activeFilm}
      isError={false}
      isFetching={false}
      loadFilms={loadFilms}
    />
  ))
  .add('films fetching', () => (
    <MoviesList
      films={[]}
      activeFilm={activeFilm}
      isError={false}
      isFetching
      loadFilms={loadFilms}
    />
  ))
  .add('films now download', () => (
    <MoviesList
      films={films}
      activeFilm={activeFilm}
      isError={false}
      isFetching={false}
      loadFilms={loadFilms}
    />
  ))
  .add('some problems with server', () => (
    <MoviesList
      films={[]}
      activeFilm={activeFilm}
      isError
      isFetching={false}
      loadFilms={loadFilms}
    />
  ));
