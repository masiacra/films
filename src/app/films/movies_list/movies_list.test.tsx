import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, ReactWrapper } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import MoviesList from './movies_list.component';
import FilmCard from './film_card/film_card.component';
import { Film } from '../films.types';
import { MoviesListProps } from './movies_list.types';

describe('MoviesList', () => {
  const listOfFilms: Film[] = [
    {
      id: 1,
      title: '',
      tagline: '',
      vote_average: 7,
      genres: [],
      release_date: '',
      poster_path: '',
      overview: '',
      budget: 3,
      revenue: 2,
      runtime: 120,
    }, {
      id: 2,
      title: '',
      tagline: '',
      vote_average: 7,
      genres: [],
      release_date: '',
      poster_path: '',
      overview: '',
      budget: 3,
      revenue: 2,
      runtime: 120,
    },
  ];
  const fakeLoadFilms = () => {};
  let wrapper: ReactWrapper;
  afterEach(() => {
    if (JSON.stringify(wrapper) !== JSON.stringify({})) {
      wrapper.unmount();
    }
  });
  const createWrapper = ({
    isError,
    isFetching,
    films,
    activeFilm,
    loadFilms,
  }: MoviesListProps) => {
    wrapper = mount(
      <MemoryRouter>
        <MoviesList
          isError={isError}
          isFetching={isFetching}
          films={films}
          activeFilm={activeFilm}
          loadFilms={loadFilms}
        />
      </MemoryRouter>,
    );
    const text = wrapper.text.bind(wrapper);
    const find = wrapper.find.bind(wrapper);
    return { text, find };
  };
  it('should apologizes when prop isError is true', () => {
    const { text } = createWrapper({
      isError: true,
      isFetching: false,
      films: [],
      activeFilm: null,
      loadFilms: fakeLoadFilms,
    });
    expect(text()).toMatch(/sorry/i);
  });
  it('should display loader when prop isFetching is true', () => {
    const { text } = createWrapper({
      isError: false,
      isFetching: true,
      films: [],
      activeFilm: null,
      loadFilms: fakeLoadFilms,
    });
    expect(text()).toMatch(/loading\.\.\./i);
  });
  it('should display FilmCards when list of films is not empty', () => {
    const { find } = createWrapper({
      isError: false,
      isFetching: false,
      films: listOfFilms,
      activeFilm: null,
      loadFilms: fakeLoadFilms,
    });
    expect(find(FilmCard)).toHaveLength(listOfFilms.length);
  });

  it('should display "No film found" when list of films is empty', () => {
    const { find, text } = createWrapper({
      isError: false,
      isFetching: false,
      films: [],
      activeFilm: null,
      loadFilms: fakeLoadFilms,
    });
    expect(find(FilmCard)).toHaveLength(0);
    expect(text()).toMatch(/no films found/i);
  });
});
