import React, { useEffect, ReactElement } from 'react';
import { Link, useParams } from 'react-router-dom';
import styled from 'styled-components';
import {
  isValidActiveFilm,
} from '../films.service';
import FilmCard from './film_card/film_card.component';
import { MoviesListProps } from './movies_list.types';
import FilmsPaths from '../../app.types';
import { SearchBy } from '../../reducer/reducer.types';

const ERROR_MESSAGE = 'Sorry, some problems with our server';
const LOADER = 'Loading...';
const NO_FILMS_FOUND = 'NO FILMS FOUND';

const FilmLink = styled(Link)`
  color: black;
  text-decoration: none;
  flex: 0 1 300px;
  margin-top: 20px;
  margin-right: 50px;
  :nth-of-type(3n) {
    margin-right: 0;
  }
`;

export default function MoviesList({
  films,
  isFetching,
  isError,
  loadFilms,
  activeFilm,
}: MoviesListProps) {
  const { queryStr } = useParams();
  useEffect(() => {
    if (queryStr) {
      const searchParams = new URLSearchParams(queryStr);
      const search = searchParams.get('Search');
      const searchBy = searchParams.get('searchBy') as SearchBy;
      loadFilms({ search, searchBy });
    } else if (isValidActiveFilm(activeFilm)) {
      const { genres } = activeFilm;
      loadFilms({ search: genres[0], searchBy: SearchBy.genre });
    }
  }, [queryStr, activeFilm, loadFilms]);
  let content: ReactElement;

  if (isError) {
    content = <h1>{ ERROR_MESSAGE }</h1>;
  } else if (isFetching) {
    content = <p>{ LOADER }</p>;
  } else if (!isFetching && films.length > 0) {
    content = (
      <>
        {films.map((film) => (
          <FilmLink key={film.id} to={`${FilmsPaths.film}/${film.id}`}>
            <FilmCard film={film} />
          </FilmLink>
        ))}
      </>
    );
  } else {
    content = <p>{ NO_FILMS_FOUND }</p>;
  }

  return content;
}
