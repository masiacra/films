import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import MoviesList from './movies_list.container';
import FilmsPaths from '../../app.types';

const Wrapper = styled.main`
  max-width: 1000px;
  margin: 0 auto;
  flex: 1 0 auto;
`;

const Content = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

export default function MoviesListWrapper() {
  return (
    <Wrapper>
      <Content>
        <Switch>
          <Route exact path={FilmsPaths.default}>
            <p />
          </Route>
          <Route path={`${FilmsPaths.search}/:queryStr`}>
            <MoviesList />
          </Route>
          <Route path={`${FilmsPaths.film}/:id`}>
            <MoviesList />
          </Route>
        </Switch>
      </Content>
    </Wrapper>
  );
}
