import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { createSelector } from 'reselect';
import { ReduxState, SortBy } from '../../reducer/reducer.types';
import MoviesList from './movies_list.component';
import { loadFilms } from '../../actions/films.resource';
import { FilmsParams } from '../../actions/films.resource.types';
import {
  sortByRating,
  sortByReleaseDate,
} from '../films.service';
import { Film } from '../films.types';

const getSortBy = ({ sortBy }: ReduxState): SortBy => sortBy;
const getFilms = ({ films }: ReduxState): Film[] => films;
const getIsFetching = ({ isFetching }: ReduxState): boolean => isFetching;
const getIsError = ({ isError }: ReduxState): boolean => isError;
const getActiveFilm = ({ activeFilm }: ReduxState): Film | null => activeFilm;

const getSortedFilms = (sortBy: SortBy, films: Film[]) => (sortBy === SortBy.rating
  ? sortByRating(films)
  : sortByReleaseDate(films));

const memoizedGetSortedFilms = createSelector([getSortBy, getFilms], getSortedFilms);

const mapStateToProps = (state: ReduxState) => ({
  films: memoizedGetSortedFilms(state),
  isFetching: getIsFetching(state),
  isError: getIsError(state),
  activeFilm: getActiveFilm(state),
});

const mapDispatchToProps = (dispatch: ThunkDispatch<ReduxState, undefined, AnyAction>) => ({
  loadFilms: (filmParams: FilmsParams) => dispatch(loadFilms(filmParams)),
});


export default connect(mapStateToProps, mapDispatchToProps)(MoviesList);
