import { Film } from '../films.types';
import { FilmsParams } from '../../actions/films.resource.types';

export interface MoviesListProps {
  films: Film[];
  isFetching: boolean;
  isError: boolean;
  loadFilms: (filmsParams: FilmsParams) => void;
  activeFilm: Film | null;
}
