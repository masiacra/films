import React from 'react';

import {
  FilmsErrorBoundaryProps,
  FilmsErrorBoundaryState,
} from './films_error_boundary.types';

const ERROR_CONTENT = 'Oops, something goes wrong. Please, contact our technical support!';

export default class FilmsErrorBoundary
  extends React.Component<FilmsErrorBoundaryProps, FilmsErrorBoundaryState> {
  constructor(props: FilmsErrorBoundaryProps) {
    super(props);
    this.state = {
      error: false,
    };
  }

  static getDerivedStateFromError(error: Error) {
    return {
      error,
    };
  }

  render() {
    const { error } = this.state;
    const { children } = this.props;
    if (error) {
      return (
        <h1>
          { ERROR_CONTENT }
        </h1>
      );
    }
    return children;
  }
}
