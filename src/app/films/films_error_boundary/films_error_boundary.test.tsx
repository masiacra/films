import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, ReactWrapper } from 'enzyme';
import FilmsErrorBoundary from './films_error_boundary.component';
import Footer from '../../footer/footer.component';

describe('FilmsErrorBoundary', () => {
  let wrapper: ReactWrapper;
  beforeEach(() => {
    wrapper = mount(
      <FilmsErrorBoundary>
        <Footer />
      </FilmsErrorBoundary>,
    );
  });
  afterEach(() => {
    wrapper.unmount();
  });

  it('should render its children', () => {
    const footer = wrapper.find('footer');
    expect(footer).toHaveLength(1);
    expect(footer.text()).toEqual('netflixroullete');
  });

  it('should display error message when appears error', () => {
    wrapper.find(Footer).simulateError(new Error('some problems'));
    expect(wrapper.text()).toEqual('Oops, something goes wrong. Please, contact our technical support!');
  });
});
