import { ReactElement } from 'react';

export interface FilmsErrorBoundaryProps {
  children: ReactElement;
}

export interface FilmsErrorBoundaryState {
  error: boolean | Error;
}
