import { SearchBy } from '../../../reducer/reducer.types';

export interface SearchFilterProps {
  searchBy: SearchBy;
  labels: SearchBy[];
  setSearchBy: (name: SearchBy) => void;
}
