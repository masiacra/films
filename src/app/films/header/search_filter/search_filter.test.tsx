import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import SearchFilter from './search_filter.component';
import SearchFilterButton from './search_filter_button/search_filter_button.component';
import { SearchBy } from '../../../reducer/reducer.types';

describe('SearchFilter', () => {
  const props = {
    onClick: () => {},
    searchBy: SearchBy.title,
    labels: [SearchBy.title, SearchBy.genre],
  };
  const wrapper = shallow(
    <SearchFilter
      setSearchBy={props.onClick}
      searchBy={props.searchBy}
      labels={props.labels}
    />,
  );
  it('should render title "Search By"', () => {
    expect(wrapper.childAt(0).text()).toMatch(/Search by/i);
  });
  it('should render the number of SearchFilterButtons are equal to length of prop labels', () => {
    expect(wrapper.find(SearchFilterButton)).toHaveLength(props.labels.length);
  });
});
