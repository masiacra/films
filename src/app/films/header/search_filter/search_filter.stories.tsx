import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import SearchFilter from './search_filter.component';
import { SearchBy } from '../../../reducer/reducer.types';

const labels: SearchBy[] = [SearchBy.title, SearchBy.genre];
const setSearchBy = action('click SearchFilter');

storiesOf('SearchFilter', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add('with 2 buttons and active button title', () => (
    <SearchFilter
      labels={labels}
      searchBy={SearchBy.title}
      setSearchBy={setSearchBy}
    />
  ))
  .add('with 2 buttons and active button genre', () => (
    <SearchFilter
      labels={labels}
      searchBy={SearchBy.genre}
      setSearchBy={setSearchBy}
    />
  ));
