import { SearchBy } from '../../../../reducer/reducer.types';

export interface SearchFilterButtonProps {
  label: SearchBy;
  searchBy: SearchBy;
  onClick: (label: SearchBy) => void;
}
