import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { SearchFilterButtonRoot } from './search_filter_button.component';
import { SearchBy } from '../../../../reducer/reducer.types';

const onClick = action('click SearchFilterButton');


storiesOf('SearchFilterButton', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add('active button', () => (
    <SearchFilterButtonRoot
      label={SearchBy.title}
      searchBy={SearchBy.title}
      onClick={onClick}
    />
  ))
  .add('not active button', () => (
    <SearchFilterButtonRoot
      label={SearchBy.title}
      searchBy={SearchBy.genre}
      onClick={onClick}
    />
  ));
