import React, { MouseEvent, useCallback, memo } from 'react';
import styled from 'styled-components';
import {
  SearchFilterButtonProps,
} from './search_filter_button.types';

export const FilterButton = styled.button<{ isActive: boolean}>`
  background-color: ${({ isActive }) => (
    isActive
      ? '#FF0074'
      : '#BFBFBF'
  )};
  font-weight: normal;
  padding: 5px 20px;
  border-radius: 5px;
  border: none;
  text-transform: uppercase;
  color: white;
  margin-left: 20px;
`;

export const SearchFilterButtonRoot = ({
  label,
  searchBy,
  onClick,
}: SearchFilterButtonProps) => {
  const handleClick = useCallback((event: MouseEvent) => {
    event.preventDefault();
    onClick(label);
  }, [onClick, label]);

  return (
    <FilterButton
      onClick={handleClick}
      type="button"
      isActive={label === searchBy}
    >
      { label }
    </FilterButton>
  );
};

const SearchFilterButton = memo(SearchFilterButtonRoot);

export default SearchFilterButton;
