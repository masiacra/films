import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import { SearchBy } from '../../../../reducer/reducer.types';
import SearchFilterButton, { FilterButton } from './search_filter_button.component';

describe('SearchFilterButton', () => {
  const fakeOnClick = jest.fn();
  const wrapper = shallow(
    <SearchFilterButton
      label={SearchBy.title}
      searchBy={SearchBy.title}
      onClick={fakeOnClick}
    />,
  );
  const find = wrapper.find.bind(wrapper);
  const { calls } = fakeOnClick.mock;
  it('should render button', () => {
    expect(find(FilterButton)).toHaveLength(1);
  });
  it('should display label', () => {
    expect(find(FilterButton).text()).toBe(SearchBy.title);
  });
  it('should pass name on click event', () => {
    find(FilterButton).simulate('click', {
      preventDefault: () => {},
    });
    expect(calls[0][0]).toEqual(SearchBy.title);
  });
});
