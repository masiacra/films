import React from 'react';
import styled from 'styled-components';
import SearchFilterButton from './search_filter_button/search_filter_button.component';
import { SearchFilterProps } from './search_filter.types';

const SEARCH_BY = 'Search by';
const Title = styled.span`
  text-transform: uppercase;
`;

export default function SearchFilter({
  searchBy,
  labels,
  setSearchBy,
}: SearchFilterProps) {
  return (
    <>
      <Title>{ SEARCH_BY }</Title>
      {
        labels.map((label) => (
          <SearchFilterButton
            searchBy={searchBy}
            label={label}
            key={label}
            onClick={setSearchBy}
          />
        ))
      }
    </>
  );
}
