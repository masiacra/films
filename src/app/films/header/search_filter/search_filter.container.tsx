import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { setSearchBy } from '../../../actions/actions';
import { ReduxState, SearchBy } from '../../../reducer/reducer.types';

import SearchFilter from './search_filter.component';

const mapStateToProps = ({
  searchBy,
}: ReduxState) => ({
  searchBy,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setSearchBy: (name: SearchBy) => dispatch(setSearchBy(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchFilter);
