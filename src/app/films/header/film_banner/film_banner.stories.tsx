import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { MemoryRouter } from 'react-router-dom';
import FilmBanner from './film_banner.component';
import { Film } from '../../films.types';
import FilmsPaths from '../../../app.types';


const activeFilmWithoutPosterPath: Film = {
  id: 1,
  title: 'shrek',
  genres: [],
  tagline: 'some tagline',
  release_date: '2019-11-14',
  vote_average: 3,
  poster_path: null,
  overview: 'film about shrek',
  budget: 120000,
  revenue: 3,
  runtime: 120,
};

const activeFilmWithPosterPath: Film = {
  ...activeFilmWithoutPosterPath,
  poster_path: 'https://image.tmdb.org/t/p/w500/140ewbWv8qHStD3mlBDvvGd0Zvu.jpg',
};

const loadActiveFilm = action('loading activeFilm');

storiesOf('FilmBanner', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .addDecorator((story) => (
    <MemoryRouter initialEntries={[`${FilmsPaths.film}/1`]}>
      {story()}
    </MemoryRouter>
  ))
  .add('with activeFilm without poster_path', () => (
    <FilmBanner
      activeFilm={activeFilmWithoutPosterPath}
      isActiveFilmError={false}
      isActiveFilmFetching={false}
      loadActiveFilm={loadActiveFilm}
    />
  ))
  .add('with activeFilm with poster_path', () => (
    <FilmBanner
      activeFilm={activeFilmWithPosterPath}
      isActiveFilmError={false}
      isActiveFilmFetching={false}
      loadActiveFilm={loadActiveFilm}
    />
  ))
  .add('fetching film', () => (
    <FilmBanner
      activeFilm={null}
      isActiveFilmError={false}
      isActiveFilmFetching
      loadActiveFilm={loadActiveFilm}
    />
  ))
  .add('fetching lead to Error', () => (
    <FilmBanner
      activeFilm={null}
      isActiveFilmError
      isActiveFilmFetching={false}
      loadActiveFilm={loadActiveFilm}
    />
  ));
