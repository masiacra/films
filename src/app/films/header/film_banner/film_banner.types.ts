import { Film } from '../../films.types';

export interface FilmBannerProps {
  activeFilm: Film;
  isActiveFilmError: boolean;
  isActiveFilmFetching: boolean;
  loadActiveFilm: (id: string) => void;
}
