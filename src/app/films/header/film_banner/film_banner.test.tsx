import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, ReactWrapper } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import FilmBanner from './film_banner.component';
import { FilmBannerProps } from './film_banner.types';

import { Film } from '../../films.types';

describe('FilmBanner', () => {
  const film: Film = {
    id: 1,
    poster_path: 'mock path',
    title: 'mock title',
    vote_average: 4.7,
    runtime: 120,
    overview: 'mock overview',
    release_date: '2017-10-01',
    tagline: '',
    budget: 1,
    revenue: 1,
    genres: [],
  };
  let wrapper: ReactWrapper;

  const mockFn = jest.fn();
  afterEach(() => {
    if (JSON.stringify(wrapper) !== JSON.stringify({})) {
      wrapper.unmount();
    }
  });

  const createWrapper = ({
    activeFilm,
    loadActiveFilm,
    isActiveFilmError,
    isActiveFilmFetching,
  }: FilmBannerProps) => {
    wrapper = mount(
      <MemoryRouter>
        <FilmBanner
          activeFilm={activeFilm}
          loadActiveFilm={loadActiveFilm}
          isActiveFilmError={isActiveFilmError}
          isActiveFilmFetching={isActiveFilmFetching}
        />
      </MemoryRouter>,
    );
    const find = wrapper.find.bind(wrapper);
    const childAt = wrapper.childAt.bind(wrapper);
    return { find, childAt };
  };


  it('should render properly when activeFilm is not null', () => {
    const { find, childAt } = createWrapper({
      activeFilm: film,
      loadActiveFilm: mockFn,
      isActiveFilmError: false,
      isActiveFilmFetching: false,
    });
    const content = `${film.release_date.split('-')[0]} ${film.runtime} min`;
    expect(find('img')).toHaveLength(1);
    expect(find('button').text()).toMatch(/back to search/i);
    expect(find('h2').text()).toEqual(film.title);
    expect(childAt(0).childAt(0).childAt(3).text()).toEqual(film.vote_average.toString());
    expect(childAt(0).childAt(0).childAt(4).text()).toEqual(content);
    expect(find('p').text()).toEqual(film.overview);
  });
  it('should render error message, when props isActiveFilmError true', () => {
    const { childAt } = createWrapper({
      activeFilm: null,
      loadActiveFilm: mockFn,
      isActiveFilmError: true,
      isActiveFilmFetching: false,
    });
    expect(childAt(0).childAt(0).childAt(0).text()).toMatch(/error/i);
  });
  it('should render loader, when isActiveFilmFetching prop true', () => {
    const { childAt } = createWrapper({
      activeFilm: null,
      loadActiveFilm: mockFn,
      isActiveFilmError: false,
      isActiveFilmFetching: true,
    });
    expect(childAt(0).childAt(0).childAt(0).text()).toMatch(/loading/i);
  });
  it('should render message, when isActiveFilmFetching and isActiveFilmError props false '
  + 'but active film is empty', () => {
    const { childAt } = createWrapper({
      activeFilm: {} as Film,
      loadActiveFilm: mockFn,
      isActiveFilmError: false,
      isActiveFilmFetching: false,
    });
    expect(childAt(0).childAt(0).childAt(0).text()).toMatch(/film not found/i);
  });
});
