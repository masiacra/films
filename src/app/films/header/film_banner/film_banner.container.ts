import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import FilmBanner from './film_banner.component';
import { ReduxState } from '../../../reducer/reducer.types';
import { loadActiveFilm } from '../../../actions/films.resource';


const mapStateToProps = ({
  activeFilm,
  isActiveFilmError,
  isActiveFilmFetching,
}: ReduxState) => ({
  activeFilm,
  isActiveFilmError,
  isActiveFilmFetching,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<ReduxState, undefined, AnyAction>) => ({
  loadActiveFilm: (id: string) => dispatch(loadActiveFilm(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilmBanner);
