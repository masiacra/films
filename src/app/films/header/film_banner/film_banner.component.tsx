import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import styled from 'styled-components';
import {
  getYear,
  validateRuntime,
  isValidActiveFilm,
} from '../../films.service';
import { FilmBannerProps } from './film_banner.types';
import FilmsPaths from '../../../app.types';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const DEFAULT_IMAGE = require('../../../../images/mock_bison.jpg');

const ERROR_MESSAGE = 'Error!';
const LOADER = 'Loading...';
const BUTTON_NAME = 'Back to search';
const NOT_FOUND_CONTENT = 'Film not found';

const BannerImage = styled.img`
  float: left;
  max-width: 300px;
`;

const BackButton = styled.button`
  background-color: #BFBFBF;
  font-weight: normal;
  padding: 5px 20px;
  border-radius: 5px;
  border: none;
  text-transform: uppercase;
  color: white;
`;

export default function FilmBanner({
  activeFilm,
  isActiveFilmError,
  isActiveFilmFetching,
  loadActiveFilm,
}: FilmBannerProps) {
  const { id } = useParams();
  useEffect(() => {
    loadActiveFilm(id);
  }, [id, loadActiveFilm]);
  if (isActiveFilmError) {
    return <h1>{ ERROR_MESSAGE }</h1>;
  }
  if (!isActiveFilmError && isActiveFilmFetching) {
    return <h1>{ LOADER }</h1>;
  }
  if (isValidActiveFilm(activeFilm)) {
    const {
      poster_path,
      title,
      release_date,
      vote_average,
      runtime,
      overview,
    } = activeFilm;
    return (
      <>
        <Link to={FilmsPaths.default}>
          <BackButton type="button">
            { BUTTON_NAME }
          </BackButton>
        </Link>
        <BannerImage
          src={poster_path || DEFAULT_IMAGE}
          alt={title}
        />
        <h2>{title}</h2>
        <div>{vote_average}</div>
        <span>{`${getYear(release_date)} ${validateRuntime(runtime)}`}</span>
        <p>{overview}</p>
      </>
    );
  }
  if (activeFilm === null) {
    return null;
  }
  return <h1>{ NOT_FOUND_CONTENT }</h1>;
}
