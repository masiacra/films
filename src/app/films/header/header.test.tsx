import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, ReactWrapper } from 'enzyme';
import { MemoryRouter, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
// eslint-disable-next-line import/no-extraneous-dependencies
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Header from './header.component';
import FilmBanner from './film_banner/film_banner.component';
import SearchForm from './search_form/search_form.component';
import SearchFilter from './search_filter/search_filter.component';
import FilmsPaths from '../../app.types';
import { Film } from '../films.types';
import { SearchBy } from '../../reducer/reducer.types';

describe('Header', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (global as any).fetch = jest.fn(() => Promise.resolve(42));

  const searchBy = SearchBy.title;

  const activeFilm: Film = {
    id: 1,
    poster_path: null,
    title: '',
    release_date: '',
    vote_average: 1,
    runtime: 120,
    overview: '',
    tagline: '',
    budget: 3,
    revenue: 1,
    genres: [],
  };

    interface FakeState {
      activeFilm: Film | null;
      searchBy: SearchBy;
    }

    const middlewares = [thunk];

    const mockStore = configureStore(middlewares);

    let wrapper: ReactWrapper;

    const createConnectedWrapper = (state: FakeState, path: string[]) => {
      const store = mockStore(state);
      wrapper = mount(
        <Provider store={store}>
          <MemoryRouter initialEntries={path}>
            <Header />
          </MemoryRouter>
        </Provider>,
      );
      return wrapper.find.bind(wrapper);
    };

    afterEach(() => {
      if (JSON.stringify(wrapper) !== JSON.stringify({})) {
        wrapper.unmount();
      }
    });

    it(`should display SearchForm and SearchFilter when path=${FilmsPaths.default}`, () => {
      const initialState: FakeState = {
        activeFilm: null,
        searchBy,
      };
      const find = createConnectedWrapper(initialState, [FilmsPaths.default]);
      expect(find(SearchForm)).toHaveLength(1);
      expect(find(SearchFilter)).toHaveLength(1);
    });
    it(`should display SearchForm and SearchFilter when path=${FilmsPaths.search}`, () => {
      const initialState: FakeState = {
        activeFilm: null,
        searchBy,
      };
      const find = createConnectedWrapper(initialState, [FilmsPaths.search]);
      expect(find(SearchForm)).toHaveLength(1);
      expect(find(SearchFilter)).toHaveLength(1);
    });
    it(`should display FilmBanner when path=${FilmsPaths.film}/1`, () => {
      const initialState: FakeState = {
        activeFilm,
        searchBy,
      };
      const find = createConnectedWrapper(initialState, [`${FilmsPaths.film}/1`]);
      expect(find(FilmBanner)).toHaveLength(1);
    });
    it('should redirect when path="/bla"', () => {
      const initialState: FakeState = {
        activeFilm,
        searchBy,
      };
      const find = createConnectedWrapper(initialState, ['/bla']);
      expect(find(Redirect)).toHaveLength(1);
    });
});
