import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import SearchForm from './search_form/search_form.container';
import FilmBanner from './film_banner/film_banner.container';
import SearchFilter from './search_filter/search_filter.container';
import { SearchBy } from '../../reducer/reducer.types';
import FilmsPaths from '../../app.types';

const labels: SearchBy[] = [SearchBy.title, SearchBy.genre];
const TITLE = 'netflixroullete';

const Wrapper = styled.header`
  background-color: #4D4D4D;
  flex: 0 0 auto;
  color: whitesmoke;
  padding: 10px;
`;

const Content = styled.div`
  max-width: 900px;
  margin: 0 auto;
`;

const Title = styled.h1`
  color: #FF0074;
`;

export default function Header() {
  return (
    <Wrapper>
      <Content>
        <Title>{TITLE}</Title>
        <Switch>
          <Route exact path={FilmsPaths.default}>
            <SearchForm>
              <SearchFilter labels={labels} />
            </SearchForm>
          </Route>
          <Route path={FilmsPaths.search}>
            <SearchForm>
              <SearchFilter labels={labels} />
            </SearchForm>
          </Route>
          <Route path={`${FilmsPaths.film}/:id`}>
            <FilmBanner />
          </Route>
          <Route path={FilmsPaths.nothingMatch}>
            <Redirect to={FilmsPaths.notFound} />
          </Route>
        </Switch>
      </Content>
    </Wrapper>
  );
}
