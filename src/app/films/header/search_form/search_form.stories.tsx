import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import SearchForm from './search_form.component';
import { SearchBy } from '../../../reducer/reducer.types';


storiesOf('SearchForm', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .addDecorator((story) => (
    <MemoryRouter>
      <div style={{ maxWidth: '800px' }}>{story()}</div>
    </MemoryRouter>
  ))
  .add('without children', () => <SearchForm searchBy={SearchBy.title} />)
  .add('with children', () => (
    <SearchForm searchBy={SearchBy.genre}>
      <p>children</p>
    </SearchForm>
  ));
