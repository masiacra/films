import { connect } from 'react-redux';
import { ReduxState } from '../../../reducer/reducer.types';
import SearchForm from './search_form.component';

const mapStateToProps = ({ searchBy }: ReduxState) => ({ searchBy });

export default connect(mapStateToProps)(SearchForm);
