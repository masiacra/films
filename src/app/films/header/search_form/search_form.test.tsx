import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount } from 'enzyme';
// eslint-disable-next-line import/no-extraneous-dependencies
import { render, fireEvent, cleanup } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import SearchForm from './search_form.component';
import { SearchBy } from '../../../reducer/reducer.types';

describe('SearchForm', () => {
  const searchBy = SearchBy.title;
  describe('test render', () => {
    const wrapper = mount(
      <MemoryRouter>
        <SearchForm searchBy={searchBy}>
          <p>fake element</p>
        </SearchForm>
      </MemoryRouter>,
    );
    let find = wrapper.find.bind(wrapper);
    afterAll(() => {
      find = null;
      wrapper.unmount();
    });
    it('should render 1 form element', () => {
      expect(find('form')).toHaveLength(1);
    });
    it('should render 1 input type="text" element', () => {
      expect(find('input[type="text"]')).toHaveLength(1);
    });
    it('should render 1 input button element', () => {
      expect(find('button')).toHaveLength(1);
    });
    it('should render children', () => {
      expect(find('p')).toHaveLength(1);
    });
  });
  describe('test state and props changes', () => {
    it('should change text in input type="text" when user typing', () => {
      const matchingText = 'some text';
      const { getByPlaceholderText } = render(
        <MemoryRouter>
          <SearchForm searchBy={searchBy} />
        </MemoryRouter>,
      );
      fireEvent.change(getByPlaceholderText('start looking'), { target: { value: matchingText } });
      expect((getByPlaceholderText('start looking') as HTMLInputElement).value).toEqual(matchingText);
      cleanup();
    });
  });
});
