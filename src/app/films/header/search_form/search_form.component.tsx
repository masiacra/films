import React, { useState, useCallback, ChangeEvent } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { SearchFormProps } from './search_form.types';
import FilmsPaths from '../../../app.types';

const SEARCH_FORM_ID = 'searchFilm';

const LABEL_CONTENT = 'Find your movie';

const BUTTON_LABEL = 'search';

const FIELD_PLACEHOLDER = 'start looking';

const Row = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

const Block = styled.div`
  flex: 0 1 auto;
  :first-of-type {
    flex: 1 1 auto;
  }
`;

const Button = styled.button`
  background-color: #ff0074;
  font-weight: normal;
  padding: 5px 20px;
  border-radius: 5px;
  border: none;
  text-transform: uppercase;
  color: white;
  padding: 8px 40px;
`;

const Field = styled.input`
  display: block;
  outline: none;
  border-radius: 5px;
  border: none;
  border-bottom: 2px solid red;
  background-color: black;
  color: white;
  width: 100%;
  font-size: 20px;
  padding: 12px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

const Label = styled.label`
  text-transform: uppercase;
`;

export default function SearchForm({ searchBy, children }: SearchFormProps) {
  const [search, setSearch] = useState('');

  const handleChange = useCallback((event: ChangeEvent) => {
    const { value } = event.target as HTMLInputElement;
    setSearch(value);
  }, []);

  return (
    <>
      <form>
        <Label htmlFor={SEARCH_FORM_ID}>
          {LABEL_CONTENT}
          <Field
            value={search}
            onChange={handleChange}
            placeholder={FIELD_PLACEHOLDER}
            type="text"
            id={SEARCH_FORM_ID}
          />
        </Label>
        <Row>
          <Block>{children}</Block>
          <Block>
            <Link
              to={`${FilmsPaths.search}/Search=${search}&searchBy=${searchBy}`}
            >
              <Button type="submit">{BUTTON_LABEL}</Button>
            </Link>
          </Block>
        </Row>
      </form>
    </>
  );
}
