import { ReactNode } from 'react';
import { SearchBy } from '../../../reducer/reducer.types';

export interface SearchFormProps {
  searchBy: SearchBy;
  children?: ReactNode;
}
