import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import NotFound from './not_found.component';

it('NotFound should display message', () => {
  const wrapper = shallow(<NotFound />);
  expect(wrapper.text()).toMatch(/404 bad request/i);
});
