import React from 'react';

const CONTENT = '404 bad request';

export default function NotFound() {
  return (
    <h1>{ CONTENT }</h1>
  );
}
