import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ReduxState, SortBy } from '../../reducer/reducer.types';
import { setSortBy } from '../../actions/actions';
import TitleBar from './title_bar.component';

const mapStateToProps = ({
  films,
  sortBy,
  activeFilm,
}: ReduxState) => ({
  films,
  sortBy,
  activeFilm,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setSortBy: (label: SortBy) => dispatch(setSortBy(label)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TitleBar);
