import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import SortFilmsActions from './sort_films_actions/sort_films_actions.component';
import FilmsPaths from '../../app.types';
import { isValidActiveFilm } from '../films.service';
import { TitleBarProps } from './title_bar.types';
import { SortBy } from '../../reducer/reducer.types';

const labels: SortBy[] = [SortBy.release_date, SortBy.rating];

const TitleBarWrapper = styled.div`
  background-color: #E5E5E5;
  flex: 0 0 auto;
  padding: 10px;
`;

const TitleBarContent = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  display: flex;
  flex-flow: row wrap;
`;

const TitleBarBlock = styled.div`
  flex: 0 1 auto;
  :first-of-type {
    flex: 1 1 auto;
  }
`;

export default function TitleBar({
  films,
  sortBy,
  setSortBy,
  activeFilm,
}: TitleBarProps) {
  const filmsLength = films.length;
  return (
    <TitleBarWrapper>
      <TitleBarContent>
        <Switch>
          <Route exact path={FilmsPaths.default}>
            <div />
          </Route>
          <Route path={FilmsPaths.search}>
            <TitleBarBlock>
              {`${filmsLength} movie${filmsLength > 1 ? 's' : ''} found`}
            </TitleBarBlock>
            <TitleBarBlock>
              <SortFilmsActions onClick={setSortBy} sortBy={sortBy} labels={labels} />
            </TitleBarBlock>
          </Route>
          <Route path={FilmsPaths.film}>
            <div>
              {isValidActiveFilm(activeFilm) ? `Films by ${activeFilm.genres[0]} genre` : ''}
            </div>
          </Route>
        </Switch>
      </TitleBarContent>
    </TitleBarWrapper>
  );
}
