import { Film } from '../films.types';
import { SortBy } from '../../reducer/reducer.types';

export interface TitleBarProps {
  films: Film[];
  sortBy: SortBy;
  setSortBy: (sortOption: SortBy) => void;
  activeFilm: null | Film;
}
