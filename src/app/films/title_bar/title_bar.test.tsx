import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, ReactWrapper } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import TitleBar from './title_bar.component';
import SortFilmsActions from './sort_films_actions/sort_films_actions.component';
import { Film } from '../films.types';
import FilmsPaths from '../../app.types';
import { SortBy } from '../../reducer/reducer.types';

describe('TitleBar', () => {
  const activeFilmParam: Film = {
    id: 1,
    title: '',
    tagline: '',
    vote_average: 7,
    genres: ['comedy'],
    release_date: '',
    poster_path: '',
    overview: '',
    budget: 3,
    revenue: 2,
    runtime: 120,
  };
  const listOfFilms: Film[] = [
    {
      id: 2,
      title: '',
      tagline: '',
      vote_average: 7,
      genres: [],
      release_date: '',
      poster_path: '',
      overview: '',
      budget: 3,
      revenue: 2,
      runtime: 120,
    },
  ];
  const sortByParam = SortBy.rating;
  const fakeSetSortBy = jest.fn();
  const { calls } = fakeSetSortBy.mock;

  let wrapper: ReactWrapper;

  interface CreateRouteredWrapperProps {
    sortBy: SortBy;
    films: Film[];
    activeFilm: Film | null;
    setSortBy: () => void;
    path: string[];
  }

  const createRouteredWrapper = ({
    sortBy,
    films,
    activeFilm,
    setSortBy,
    path,
  }: CreateRouteredWrapperProps) => {
    wrapper = mount(
      <MemoryRouter initialEntries={path}>
        <TitleBar
          sortBy={sortBy}
          films={films}
          activeFilm={activeFilm}
          setSortBy={setSortBy}
        />
      </MemoryRouter>,
    );
    const find = wrapper.find.bind(wrapper);
    const text = wrapper.text.bind(wrapper);
    return { find, text };
  };

  afterEach(() => {
    if (JSON.stringify(wrapper) !== JSON.stringify({})) {
      wrapper.unmount();
    }
  });

  it(`should render content with phrase "Films by ${activeFilmParam.genres[0]} genre" when path='${FilmsPaths.film}'`,
    () => {
      const { text } = createRouteredWrapper({
        path: [FilmsPaths.film],
        sortBy: sortByParam,
        films: listOfFilms,
        activeFilm: activeFilmParam,
        setSortBy: fakeSetSortBy,
      });

      expect(text()).toMatch(/films by comedy genre/i);
    });
  describe(`when path="${FilmsPaths.search}"`, () => {
    const { find } = createRouteredWrapper({
      path: [FilmsPaths.search],
      sortBy: sortByParam,
      films: listOfFilms,
      activeFilm: null,
      setSortBy: fakeSetSortBy,
    });
    it('should render ResultsSort component', () => {
      expect(find(SortFilmsActions)).toHaveLength(1);
    });
    it('should handleClick on button', () => {
      find('button').first().simulate('click');
      expect(calls.length).toEqual(1);
      expect(calls[0][0]).toEqual('release date');
    });
  });

  it(`should render empty element when path="${FilmsPaths.default}"`, () => {
    const { text } = createRouteredWrapper({
      path: [FilmsPaths.default],
      sortBy: sortByParam,
      films: listOfFilms,
      activeFilm: null,
      setSortBy: fakeSetSortBy,
    });
    expect(text()).toEqual('');
  });
});
