import React, { memo } from 'react';
import SortFilmsButton from './sort_films_button/sort_films_button.component';
import { SortFilmsActionsProps } from './sort_films_actions.types';
import { SortBy } from '../../../reducer/reducer.types';

const SORT_BY = 'Sort by';

export const SortFilmsActionsRoot = ({
  sortBy,
  onClick,
  labels,
}: SortFilmsActionsProps) => (
  <div>
    <span>{ SORT_BY }</span>
    {
        labels.map((label: SortBy) => (
          <SortFilmsButton
            label={label}
            sortBy={sortBy}
            onClick={onClick}
            key={label}
          />
        ))
      }
  </div>
);

const SortFilmsActions = memo(SortFilmsActionsRoot);

export default SortFilmsActions;
