import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import { SortBy } from '../../../reducer/reducer.types';
import SortFilmsActions from './sort_films_actions.component';
import SortFilmsButton from './sort_films_button/sort_films_button.component';

describe('SortFilmsActions', () => {
  const labels = [SortBy.rating, SortBy.release_date];
  const wrapper = shallow(
    <SortFilmsActions
      sortBy={SortBy.rating}
      onClick={() => {}}
      labels={labels}
    />,
  );
  it('should render 2 SortFilmsButton', () => {
    expect(wrapper.find(SortFilmsButton)).toHaveLength(labels.length);
  });
  it('should display phrase sort by', () => {
    expect(wrapper.childAt(0).text()).toMatch(/sort by/i);
  });
});
