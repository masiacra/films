import { SortBy } from '../../../../reducer/reducer.types';

export interface SortFilmsButtonProps {
  sortBy: SortBy;
  label: SortBy;
  onClick: (label: SortBy) => void;
}
