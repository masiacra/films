import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow } from 'enzyme';
import { SortBy } from '../../../../reducer/reducer.types';
import SortFilmsButton, { FilmsButton } from './sort_films_button.component';

describe('SortFilmsButton', () => {
  const fakeOnClick = jest.fn();
  const { calls } = fakeOnClick.mock;
  const wrapper = shallow(
    <SortFilmsButton
      label={SortBy.rating}
      sortBy={SortBy.release_date}
      onClick={fakeOnClick}
    />,
  );
  const find = wrapper.find.bind(wrapper);
  it('should render button', () => {
    expect(find(FilmsButton)).toHaveLength(1);
  });
  it('should display label', () => {
    expect(find(FilmsButton).text()).toBe(SortBy.rating);
  });
  it('should pass name on click event', () => {
    find(FilmsButton).simulate('click', {
      preventDefault: () => { },
    });
    expect(calls[0][0]).toEqual(SortBy.rating);
  });
});
