import React, { useCallback, MouseEvent, memo } from 'react';
import styled from 'styled-components';
import { SortFilmsButtonProps } from './sort_films_button.types';

export const FilmsButton = styled.button<{ isActive: boolean }>`
  border: none;
  background-color: transparent;
  margin-left: 10px;
  color: ${({ isActive }) => (
    isActive
      ? '#FF0074'
      : 'blueviolet'
  )};
  font-size: 18px;

  :hover {
    color: yellowgreen;
  }
`;

export const SortFilmsButtonRoot = ({
  sortBy,
  label,
  onClick,
}: SortFilmsButtonProps) => {
  const handleClick = useCallback((event: MouseEvent) => {
    event.preventDefault();
    onClick(label);
  }, [onClick, label]);
  return (
    <FilmsButton
      isActive={sortBy === label}
      onClick={handleClick}
      type="button"
      tabIndex={0}
    >
      { label }
    </FilmsButton>
  );
};

const SortFilmsButton = memo(SortFilmsButtonRoot);

export default SortFilmsButton;
