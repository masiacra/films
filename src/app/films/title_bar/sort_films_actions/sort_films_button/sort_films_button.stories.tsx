import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { SortFilmsButtonRoot } from './sort_films_button.component';
import { SortBy } from '../../../../reducer/reducer.types';

const onClick = action('click ResultsSortButton');

storiesOf('SortFilmsButton', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add('active button', () => (
    <SortFilmsButtonRoot
      label={SortBy.release_date}
      sortBy={SortBy.release_date}
      onClick={onClick}
    />
  ))
  .add('not active button', () => (
    <SortFilmsButtonRoot
      label={SortBy.rating}
      sortBy={SortBy.release_date}
      onClick={onClick}
    />
  ));
