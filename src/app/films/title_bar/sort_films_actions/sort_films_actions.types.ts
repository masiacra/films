import { SortBy } from '../../../reducer/reducer.types';

export interface SortFilmsActionsProps {
  sortBy: SortBy;
  labels: SortBy[];
  onClick: (name: SortBy) => void;
}
