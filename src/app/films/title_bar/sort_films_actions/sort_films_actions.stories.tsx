import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { SortFilmsActionsRoot } from './sort_films_actions.component';
import { SortBy } from '../../../reducer/reducer.types';

const labels: SortBy[] = [SortBy.release_date, SortBy.rating];
const onClick = action('click ResultsSort');

storiesOf('SortFilmsActions', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add(`with 2 buttons and active button ${SortBy.release_date}`, () => (
    <SortFilmsActionsRoot
      labels={labels}
      sortBy={SortBy.release_date}
      onClick={onClick}
    />
  ))
  .add(`with 2 buttons and active button ${SortBy.rating}`, () => (
    <SortFilmsActionsRoot
      labels={labels}
      sortBy={SortBy.rating}
      onClick={onClick}
    />
  ));
