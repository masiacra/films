import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
import Footer from './footer.component';

storiesOf('Footer', module)
  .addParameters({
    info: {
      inline: true,
      header: true,
      propTables: false,
    },
  })
  .add('default state', () => (
    <Footer />
  ));
