import React from 'react';
import styled from 'styled-components';

const LOGO = 'netflixroullete';

const Content = styled.footer`
  background-color: #BFBFBF;
  text-align: center;
  color: #FF0074;
`;

export default function Footer() {
  return (
    <Content>
      { LOGO }
    </Content>
  );
}
