import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
// eslint-disable-next-line import/no-extraneous-dependencies

import Footer from './footer.component';

describe('Footer', () => {
  it('should renders correctly', () => {
    const div = document.createElement('div');
    render(<Footer />, div);
    unmountComponentAtNode(div);
  });
});
