import { createStore, applyMiddleware, AnyAction } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducer from '../reducer/reducer';
import { ReduxState } from '../reducer/reducer.types';

const persistConfig = {
  key: 'netflixroullete',
  storage,
  blacklist: ['isError', 'isActiveFilmError'],
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = createStore(
  persistedReducer,
  applyMiddleware(thunk as ThunkMiddleware<ReduxState, AnyAction>),
);
export const persistor = persistStore(store);
