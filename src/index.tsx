import React from 'react';
import { render } from 'react-dom';
import App from './app/app.component';

import './reset.css';
import './index.css';

render(
  <App />,
  document.getElementById('films-root'),
);
