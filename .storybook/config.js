import { configure } from '@storybook/react';
import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

configure(require.context('../src', true, /\.stories\.tsx$/), module);
addDecorator(withInfo);
